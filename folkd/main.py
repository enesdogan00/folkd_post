from time import sleep
from pydantic import BaseModel
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ChromeOptions

class Post(BaseModel):
    url: str
    title: str
    desc: str
    tags: list[str]

class Folkd:
    def __init__(self,email,password, headless=True):
        options = ChromeOptions()
        options.add_argument('--no-sandbox')
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--disable-gpu")  # Disable GPU usage
        options.add_argument("--window-size=1920x1080")  # Set a window sizes

        if headless:
            options.add_argument("--headless=new")
        self.email = email
        self.password = password
        self.driver = webdriver.Chrome(options=options)
        self.driver.set_window_size(1920, 1080)
        self.wait = WebDriverWait(self.driver, 60)
        self.login()

    def login(self) -> bool:
        try:
            self.driver.get('https://folkd.com/')
            self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'Login')]"))).click()
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@type='email']")))\
                .send_keys(self.email)
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@type='password']")))\
                .send_keys(self.password)
            self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Login')]"))).click()
            self.wait.until(EC.presence_of_element_located((By.XPATH, "//div[contains(text(), 'Log out')]")))
            return True
        except TimeoutException:
            self.driver.save_screenshot('/tmp/error.png')
            raise Exception('Login failed')
    
    def send_post(self, post: Post) -> str:
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[contains(text(), 'New link')]"))).click()
        self.wait.until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='https://folkd.com']"))).send_keys(post.url)
        self.wait.until(EC.presence_of_element_located((By.ID, "linktitle"))).send_keys(post.title)
        self.wait.until(EC.presence_of_element_located((By.ID, "linkdescription"))).send_keys(post.desc)
        # for tag in post.tags:
        #     self.wait.until(EC.presence_of_element_located((By.ID, "tagElementCrud"))).send_keys(tag)
        #     self.wait.until(EC.element_to_be_clickable((By.XPATH, "//div[@id='groupFocusTagsLink']//div[contains(text(), 'Add')]"))).click()    
        sleep(.5)
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(.5)
        self.wait.until(EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), 'Add')]"))).click()
        self.wait.until(lambda driver: len(driver.window_handles) > 1)
        self.driver.switch_to.window(self.driver.window_handles[-1])
        return self.driver.current_url
